package net.strainovic.tagged.taggedbox.controller;

import com.dropbox.core.v2.users.IndividualSpaceAllocation;
import com.dropbox.core.v2.users.SpaceAllocation;
import com.dropbox.core.v2.users.SpaceUsage;
import net.strainovic.tagged.taggedbox.controller.util.TokenExtractor;
import net.strainovic.tagged.taggedbox.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    private TokenExtractor tokenExtractor;

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getUserSpaceUsageTest() throws Exception {
        final String token = "Bearer dummyToken";
        when(tokenExtractor.extractHeaderToken(any(HttpServletRequest.class))).thenReturn(token);

        IndividualSpaceAllocation allocation = new IndividualSpaceAllocation(100L);
        SpaceAllocation spaceAllocation = SpaceAllocation.individual(allocation);

        SpaceUsage spaceUsage = new SpaceUsage(1L, spaceAllocation);
        when(userService.getUserSpaceUsage(token)).thenReturn(spaceUsage);

        mvc.perform(MockMvcRequestBuilders.get("/v1/spaceUsage").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.allocated").value(100))
                .andExpect(MockMvcResultMatchers.jsonPath("$.used").value(1));
    }
}