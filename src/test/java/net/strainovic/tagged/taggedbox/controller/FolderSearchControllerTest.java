package net.strainovic.tagged.taggedbox.controller;

import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.strainovic.tagged.taggedbox.controller.command.PathCommand;
import net.strainovic.tagged.taggedbox.controller.util.TokenExtractor;
import net.strainovic.tagged.taggedbox.service.FolderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FolderSearchController.class)
public class FolderSearchControllerTest {

    @MockBean
    private FolderService folderService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TokenExtractor tokenExtractor;

    @Test
    public void getFolderContentTest() throws Exception {
        final String token = "Bearer dummyToken";
        final String path = "";

        when(tokenExtractor.extractHeaderToken(any(HttpServletRequest.class))).thenReturn(token);

        Metadata file1 = new FileMetadata("File1", "fileId1", new Date(), new Date(), "123456789", 1000L, "/", "/", null, null, null, null, Boolean.FALSE, null);
        Metadata folder1 = new FolderMetadata("Folder1", "folderId", "/", "/", null, null, null, null);

        List<Metadata> entries = Arrays.asList(file1, folder1);
        ListFolderResult listFolderResult = new ListFolderResult(entries, "cursor", false);

        when(folderService.listFolder(token, path)).thenReturn(listFolderResult);

        mvc.perform(MockMvcRequestBuilders.post("/v1/folder")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(new PathCommand(path))))
                .andExpect(status().isOk())

                .andExpect(MockMvcResultMatchers.jsonPath("[0].fileId").value("fileId1"))
                .andExpect(MockMvcResultMatchers.jsonPath("[0].folder").value(false))
                .andExpect(MockMvcResultMatchers.jsonPath("[0].name").value("File1"))
                .andExpect(MockMvcResultMatchers.jsonPath("[0].path").value("/"))

                .andExpect(MockMvcResultMatchers.jsonPath("[1].fileId").value("folderId"))
                .andExpect(MockMvcResultMatchers.jsonPath("[1].folder").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("[1].name").value("Folder1"))
                .andExpect(MockMvcResultMatchers.jsonPath("[1].path").value("/"));
    }
}