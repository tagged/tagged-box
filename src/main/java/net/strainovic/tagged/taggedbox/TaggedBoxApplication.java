package net.strainovic.tagged.taggedbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaggedBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaggedBoxApplication.class, args);
	}
}
