package net.strainovic.tagged.taggedbox.service;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.ListFolderResult;
import org.springframework.stereotype.Service;

@Service
public class FolderServiceImpl implements FolderService {

    @Override
    public ListFolderResult listFolder(String accessToken, String path) {
        //If root / is specified convert it to empty string
        if ("/".equals(path)) {
            path = "";
        }

        DbxRequestConfig config = new DbxRequestConfig("tagged-box");
        DbxClientV2 client = new DbxClientV2(config, accessToken);

        // Get files and folder metadata from Dropbox root directory
        try {
            return client.files().listFolder(path);
        } catch (DbxException e) {
            throw new RuntimeException("Unable to list folder", e);
        }
    }

    @Override
    public ListFolderResult listFolderContinue(String accessToken, String cursor) {
        DbxRequestConfig config = new DbxRequestConfig("tagged-box");
        DbxClientV2 client = new DbxClientV2(config, accessToken);

        // Get files and folder metadata from Dropbox root directory
        try {
            return client.files().listFolderContinue(cursor);
        } catch (DbxException e) {
            throw new RuntimeException("Unable to continue list folder", e);
        }    }
}
