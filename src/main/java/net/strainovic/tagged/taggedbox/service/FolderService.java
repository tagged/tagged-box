package net.strainovic.tagged.taggedbox.service;

import com.dropbox.core.v2.files.ListFolderResult;

public interface FolderService {

    ListFolderResult listFolder(String accessToken, String path);

    ListFolderResult listFolderContinue(String accessToken, String cursor);
}
