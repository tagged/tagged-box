package net.strainovic.tagged.taggedbox.service;

import com.dropbox.core.v2.users.SpaceUsage;

public interface UserService {

    SpaceUsage getUserSpaceUsage(String accessToken);
}
