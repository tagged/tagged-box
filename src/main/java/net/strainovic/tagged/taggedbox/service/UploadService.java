package net.strainovic.tagged.taggedbox.service;

import net.strainovic.tagged.taggedbox.controller.dto.FileDto;
import org.springframework.web.multipart.MultipartFile;

public interface UploadService {

    FileDto uploadFile(String accessToken, MultipartFile multipartFile, String dropboxPath);
}
