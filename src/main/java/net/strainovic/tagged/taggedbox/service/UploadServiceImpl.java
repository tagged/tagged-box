package net.strainovic.tagged.taggedbox.service;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.CommitInfo;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadSessionCursor;
import com.dropbox.core.v2.files.WriteMode;
import com.google.common.io.Files;
import net.strainovic.tagged.taggedbox.controller.dto.FileDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

@Service
public class UploadServiceImpl implements UploadService {

    private static final long CHUNKED_UPLOAD_CHUNK_SIZE = 8L << 20; // 8MiB

    @Override
    public FileDto uploadFile(String accessToken, MultipartFile multipartFile, String dropboxPath) {
        File file = multipartToTempFile(multipartFile);

        DbxRequestConfig config = new DbxRequestConfig("tagged-box");
        DbxClientV2 client = new DbxClientV2(config, accessToken);

        FileMetadata metadata;

        if (file.length() <= (2 * CHUNKED_UPLOAD_CHUNK_SIZE)) {
            metadata = singleUpload(client, file, dropboxPath);
        } else {
            metadata = chunkedUploadFile(client, file, dropboxPath);
        }

        FileDto fileDto = new FileDto();
        fileDto.setName(metadata.getName());
        fileDto.setPath(metadata.getPathDisplay());
        fileDto.setFileId(metadata.getId());
        fileDto.setModified(metadata.getClientModified());
        fileDto.setFolder(false);

        return fileDto;
    }

    /**
     * Chunked uploads have 3 phases, each of which can accept uploaded bytes:
     * <p>
     * (1)  Start: initiate the upload and get an upload session ID
     * (2) Append: upload chunks of the file to append to our session
     * (3) Finish: commit the upload and close the session
     * <p>
     * We track how many bytes we uploaded to determine which phase we should be in.
     * <p>
     * This is basic implementation with no retry options
     *
     * @param client      - Dropbox client
     * @param file        - file for upload
     * @param dropboxPath - path on dropbox where file should be saved
     * @return uploaded file metadata
     */
    private FileMetadata chunkedUploadFile(DbxClientV2 client, File file, String dropboxPath) {
        long size = file.length();

        long uploaded = 0L;
        try (InputStream in = new FileInputStream(file)) {
            // if this is a retry, make sure seek to the correct offset
            in.skip(uploaded);

            // (1) Start
            String sessionId = client.files().uploadSessionStart()
                    .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE)
                    .getSessionId();
            uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;


            UploadSessionCursor cursor = new UploadSessionCursor(sessionId, uploaded);

            // (2) Append
            while ((size - uploaded) > CHUNKED_UPLOAD_CHUNK_SIZE) {
                client.files().uploadSessionAppendV2(cursor)
                        .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE);
                uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
                cursor = new UploadSessionCursor(sessionId, uploaded);
            }

            // (3) Finish
            long remaining = size - uploaded;
            CommitInfo commitInfo = CommitInfo.newBuilder(dropboxPath)
                    .withMode(WriteMode.ADD)
                    .withClientModified(new Date(file.lastModified()))
                    .build();
            return client.files().uploadSessionFinish(cursor, commitInfo)
                    .uploadAndFinish(in, remaining);


        } catch (DbxException | IOException e) {
            throw new RuntimeException("Error uploading to Dropbox: " + e.getMessage());
        }
    }

    private File multipartToTempFile(MultipartFile multipart) {
        String originalFilename = multipart.getOriginalFilename();

        File tempDir = Files.createTempDir();

        File file = new File(tempDir.getAbsolutePath(), originalFilename);
        try {
            multipart.transferTo(file);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Error during converting multipart to file with name %s", originalFilename), e);
        }
        return file;
    }

    private FileMetadata singleUpload(DbxClientV2 client, File file, String dropboxPath) {
        try (InputStream is = new FileInputStream(file)) {
            return client.files().uploadBuilder(dropboxPath)
                    .withMode(WriteMode.OVERWRITE)
                    .withClientModified(new Date(file.lastModified()))
                    .uploadAndFinish(is);
        } catch (IOException | DbxException e) {
            throw new RuntimeException(String.format("Error uploading file %s to Dropbox: %s", file.getName(), e.getMessage()), e);
        }
    }
}
