package net.strainovic.tagged.taggedbox.service;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.SpaceUsage;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public SpaceUsage getUserSpaceUsage(String accessToken) {
        DbxRequestConfig config = new DbxRequestConfig("tagged-box");
        DbxClientV2 client = new DbxClientV2(config, accessToken);

        try {
            return client.users().getSpaceUsage();
        } catch (DbxException e) {
            throw new RuntimeException("Unable to get user space usage", e);
        }
    }
}
