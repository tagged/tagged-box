package net.strainovic.tagged.taggedbox.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class SpaceUsageDto {

    @ApiModelProperty
    private Long allocated;

    @ApiModelProperty
    private Long used;

    public Long getAllocated() {

        return allocated;
    }

    public Long getUsed() {

        return used;
    }

    public void setAllocated(Long allocated) {

        this.allocated = allocated;
    }

    public void setUsed(Long used) {

        this.used = used;
    }
}
