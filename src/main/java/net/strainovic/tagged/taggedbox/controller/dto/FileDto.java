package net.strainovic.tagged.taggedbox.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel
public class FileDto {

    @ApiModelProperty
    private String fileId;

    @ApiModelProperty
    private boolean folder;

    @ApiModelProperty
    private Date modified;

    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private String path;

    public String getFileId() {
        return fileId;
    }

    public Date getModified() {
        return modified;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public boolean isFolder() {
        return folder;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setFolder(boolean folder) {
        this.folder = folder;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
