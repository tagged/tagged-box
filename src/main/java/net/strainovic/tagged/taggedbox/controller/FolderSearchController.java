package net.strainovic.tagged.taggedbox.controller;

import com.dropbox.core.DbxException;
import com.dropbox.core.InvalidAccessTokenException;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedbox.controller.command.PathCommand;
import net.strainovic.tagged.taggedbox.controller.dto.FileDto;
import net.strainovic.tagged.taggedbox.controller.util.TokenExtractor;
import net.strainovic.tagged.taggedbox.service.FolderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Api(value = "Search", description = "Getting folder list", consumes = "application/json", tags = "Folder")
@RestController
public class FolderSearchController {

    private final FolderService folderService;

    private final TokenExtractor tokenExtractor;

    @Autowired
    public FolderSearchController(TokenExtractor tokenExtractor, FolderService folderService) {
        this.tokenExtractor = tokenExtractor;
        this.folderService = folderService;
    }

    @ApiOperation(value = "Get folder list", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return folder content"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", value = "JWT", dataType = "string", format = "JWT", required = true)})
    @PostMapping("/v1/folder")
    public List<FileDto> getFolderContent(HttpServletRequest request, @RequestBody @NotNull PathCommand pathCommand) throws DbxException {
        String accessToken = tokenExtractor.extractHeaderToken(request);

        if (StringUtils.isBlank(accessToken)) {
            throw new InvalidAccessTokenException(request.getRequestedSessionId(), "Cannot access Dropbox account");
        }

        List<FileDto> files = new ArrayList<>();
        ListFolderResult result = getFolderService().listFolder(accessToken, pathCommand.getPath());
        while (true) {
            for (Metadata metadata : result.getEntries()) {
                FileDto fileDto = new FileDto();

                fileDto.setName(metadata.getName());
                fileDto.setPath(metadata.getPathDisplay());

                if (metadata instanceof FolderMetadata) {
                    FolderMetadata folderMetadata = (FolderMetadata) metadata;

                    fileDto.setFileId(folderMetadata.getId());
                    fileDto.setFolder(true);
                } else if (metadata instanceof FileMetadata) {
                    FileMetadata fileMetadata = (FileMetadata) metadata;

                    fileDto.setFileId(fileMetadata.getId());
                    fileDto.setModified(fileMetadata.getClientModified());
                    fileDto.setFolder(false);
                }
                files.add(fileDto);
            }
            if (!result.getHasMore()) {
                break;
            }
            result = getFolderService().listFolderContinue(accessToken, result.getCursor());
        }
        return files;
    }

    private FolderService getFolderService() {
        return folderService;
    }
}
