package net.strainovic.tagged.taggedbox.controller;

import com.dropbox.core.v2.DbxPathV2;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedbox.controller.dto.FileDto;
import net.strainovic.tagged.taggedbox.controller.util.TokenExtractor;
import net.strainovic.tagged.taggedbox.service.UploadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.nio.file.AccessDeniedException;

@Api(value = "Upload", description = "Upload file", consumes = "application/json", tags = "Upload")
@RestController
public class FileUploadController {

    private final TokenExtractor tokenExtractor;

    private final UploadService uploadService;

    @Autowired
    public FileUploadController(TokenExtractor tokenExtractor, UploadService uploadService) {
        this.tokenExtractor = tokenExtractor;
        this.uploadService = uploadService;
    }

    @ApiOperation(value = "Get folder list", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "File uploaded"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", value = "JWT", dataType = "string", format = "JWT", required = true)})
    @PostMapping("/v1/upload")
    public FileDto uploadFile(HttpServletRequest request, @RequestPart @Valid MultipartFile file, @Valid @RequestPart @NotNull String dropboxPath) throws AccessDeniedException {
        String accessToken = tokenExtractor.extractHeaderToken(request);

        if (StringUtils.isBlank(accessToken)) {
            throw new AccessDeniedException(null, null, "JWT token missing");
        }

        dropboxPath = dropboxPath + File.separator + file.getOriginalFilename();
        String pathError = DbxPathV2.findError(dropboxPath);

        if (StringUtils.isNotBlank(pathError)) {
            throw new RuntimeException(pathError);
        }

        return getUploadService().uploadFile(accessToken, file, dropboxPath);
    }

    private UploadService getUploadService() {
        return uploadService;
    }
}
