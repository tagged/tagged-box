package net.strainovic.tagged.taggedbox.controller;

import com.dropbox.core.v2.users.SpaceUsage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedbox.controller.dto.SpaceUsageDto;
import net.strainovic.tagged.taggedbox.controller.util.TokenExtractor;
import net.strainovic.tagged.taggedbox.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(value = "Space Usage", description = "Getting user information", consumes = "application/json", tags = "User")
@RestController
public class UserController {

    private final TokenExtractor tokenExtractor;

    private final UserService userService;

    @Autowired
    public UserController(TokenExtractor tokenExtractor, UserService userService) {
        this.tokenExtractor = tokenExtractor;
        this.userService = userService;
    }

    @ApiOperation(value = "Get folder list", httpMethod = "GET")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Return used and allocated space for user"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", value = "JWT", dataType = "string", format = "JWT", required = true)})
    @GetMapping("/v1/spaceUsage")
    public SpaceUsageDto getSpaceUsage(HttpServletRequest request) {
        String accessToken = getTokenExtractor().extractHeaderToken(request);
        SpaceUsage spaceUsage = getUserService().getUserSpaceUsage(accessToken);

        SpaceUsageDto spaceUsageDto = new SpaceUsageDto();
        spaceUsageDto.setUsed(spaceUsage.getUsed());
        spaceUsageDto.setAllocated(spaceUsage.getAllocation().getIndividualValue().getAllocated());

        return spaceUsageDto;
    }

    private TokenExtractor getTokenExtractor() {
        return tokenExtractor;
    }

    private UserService getUserService() {
        return userService;
    }
}
