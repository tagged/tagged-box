package net.strainovic.tagged.taggedbox.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class PathCommand {

    @ApiModelProperty(required = true, example = "/folder", notes = "Specify the root folder as an empty string rather than as \"/\"")
    private String path;

    public PathCommand(String path) {
        this.path = path;
    }

    public PathCommand() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
